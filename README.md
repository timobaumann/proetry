# some code and experiments for visualizing prosody in poetry

a very brief description of how both the 

## Prosub to generate alignments

Repository: https://bitbucket.org/natsuhh/prosub/

- prepare a 16kHz mono pcm WAVE audio file
- prepare a raw txt file (without title/etc.)
- put title/author/etc. (including source!) into a .meta file
		
start MaryServer instance to listen on default port (59125)

- `NAME=Hughes-Langston-The-Weary-Blues`
- `workon prosub`  
- `run.py -c config/align.json -o name=../$NAME` (filename without .wav/.txt)

## AuToBI to generate ToBI from audio

Repository: http://github.com/AndrewRosenberg/AuToBI

turn alignment coming from prosub into Wavesurfer format using sphinx2lab.pl; use Intelida's TGtool (http://bitbucket.org/inpro/intelida/) to turn it into a TextGrid.

- `./sphinx2lab.pl < prosub/$NAME/temp/aligned > $NAME.lab`
- `TGtool.pl "add $NAME.	lab words" "save $NAME.TextGrid"`

run the following; there's also another model beyond "bdc_burnc.*". No clue which of the two is better.
convention that I followed: bdc_burnc.-based results are AuToBI2.TextGrid, games.-based results are AuToBI.TextGrid

- `MODE=bdc_burnc`
- `java -jar AuToBI/AuToBI.jar -input_file=$NAME.TextGrid -words_tier_name=words -wav_file=$NAME.wav -pitch_accent_detector=AuToBI/models/$MODE.acc.detection.model -pitch_accent_classifier=AuToBI/models/$MODE.acc.classification.model -intonational_phrase_boundary_detector=AuToBI/models/$MODE.intonp.detection.model -intermediate_phrase_boundary_detector=AuToBI/models/$MODE.interp.detection.model -phrase_accent_classifier=AuToBI/models/$MODE.pabt.classification.model -phrase_accent_boundary_tone_classifier=AuToBI/models/$MODE.phacc.classification.model -out_file=$NAME.AuToBI-$MODE.TextGrid`

## MaryTTS to generate ToBI from text
- no converter for MaryXML to TextGrid yet (or any other means to convert results; do this manually if you want it)

## MaryTTS+AuToBI to generate ToBI from synthesis:
- have MaryTTS generate audio from .txt (potential room for variation: with/without line-breaks); just use web-interface :-/
- AuToBI-analyze the generated audio, compare output with output for author's rendition

## Display results

load the audio and the label-files in Wavesurfer.

## potential leverage for analysis:
- difference in total number of breaks (and their strengths)
- number of matches/mismatches between the two (would be cool to automatically analyze the mis-matches)
- difference in tone-types and break-tone-types

--> see script tobiComparison.pl








