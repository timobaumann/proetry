#!/usr/bin/perl 
#-I /home/timo/uni/projekte/intelida.git/lib
use strict;
use warnings;


# you will need to have INTELIDA installed and in your include path!
require "TextGrid.pm";
require "Alignment.pm";
require "Label.pm";

use Data::Dumper;
use List::MoreUtils qw(uniq);

$#ARGV == 1 or die "usage: tobiComparison.pl AuToBI1.TextGrid AuToBI2.TextGrid\
textgrids should be of the same text and must contain the tiers \"words\", \"breaks\" and \"tones\".\n";

my $a = TextGrid::newTextGridFromFile($ARGV[0]);
print "A:\t$ARGV[0]\n";
my $b = TextGrid::newTextGridFromFile($ARGV[1]);
print "B:\t$ARGV[1]\n";

# words contains Word-objects. These have all the (matching) words with the TOBI marks of both the A and B track attached
my @words = loadMatchingWords($a, $b);

addToBI(\@words, 'a', $a);
addToBI(\@words, 'b', $b);

printStatsFor('BI');
printStatsFor('BT');
printStatsFor('TO');
printConfusionFor('TO');

sub printStatsFor {
	my $prop = $_[0];
	my %aBIstats = getSimpleStats(\@words, 'a', $prop);
	my %bBIstats = getSimpleStats(\@words, 'b', $prop);
	print "\nOverall stats for $prop:\n";
	print "\tA:\tB:\tdiff\treldiff\tprop\n";
# reldiff is 
	foreach my $key (uniq sort (keys %aBIstats, keys %bBIstats)) {
		my $aStats = ($aBIstats{$key} or 0);
		my $bStats = ($bBIstats{$key} or 0);
		print "$key:\t$aStats\t$bStats\t"
		    . ($aStats-$bStats) . "\t"
		    . ($aStats != 0 ? sprintf("%.3f", -($aStats-$bStats)/$aStats) : "inf") . "\t"
		    . ($bStats != 0 ? sprintf("%.3f", $aStats/$bStats) : "inf") . "\n";
	}
}

sub printConfusionFor {
	my $prop = $_[0];
	my %stats = getConfusions(\@words, $prop);
	my @keys = sort keys %stats;
	print join "\t", ("", "count", @keys);
	print "\n";
	foreach my $aprop (@keys) {
		print "$aprop\t";
		print join "\t", map { $_ ? sprintf "%.3f", $_ : "0" } @{$stats{$aprop}}{('count', @keys)};
		print "\n";
	}
}

sub getConfusions {
	my ($words, $prop) = @_;
	my %conf; # we'll return a hash of hashes: the outer hash indexes by a-props and contains a hash,
		  # the inner hash indexes by b-props and contains their counts as values
	my %propStd;
	foreach my $word (@{$words}) {
		$propStd{$word->{'aprop'}->{$prop}}++;
		# create the inner hash if necessary:
		$conf{$word->{'aprop'}->{$prop}} = {} unless ($propStd{$word->{'aprop'}->{$prop}} > 1);
		$conf{$word->{'aprop'}->{$prop}}->{$word->{'bprop'}->{$prop}}++;
	}
	# now we normalize the confusion (for simplicity) and add an overall 'count' to each hash in stats
	foreach my $prop (keys %propStd) {
		map { $_ /= $propStd{$prop} } values %{$conf{$prop}};
		$conf{$prop}->{'count'} = $propStd{$prop};
		print STDERR "count of $prop is $propStd{$prop}\n";
	}
	return %conf;
}

sub getSimpleStats {
	my ($words, $ab, $prop) = @_;
	my %stats;
	foreach my $word (@{$words}) {
		$stats{$word->{$ab . "prop"}->{$prop}}++;
	}
	return %stats;
}

sub loadMatchingWords {
	my ($a, $b) = @_;
	my @words;
	my @alignmentOps = @{$a->getAlignmentByName('words')
	                       ->labelDiffToOtherAlignment(
	                         $b->getAlignmentByName('words'))};
	my $ai; my $bi; # indices for A and B words
	foreach my $op (@alignmentOps) {
		if ($op eq 'INITIAL') { $ai = 0; $bi = 0; 
		} elsif ($op eq 'MATCH') {
			push @words, Word::new(($a->getAlignmentByName('words')->getLabels())[$ai],
				               ($b->getAlignmentByName('words')->getLabels())[$bi]);
			#print( ($a->getAlignmentByName('words')->getLabels())[$ai]->{'text'}, "\t", 
			#($b->getAlignmentByName('words')->getLabels())[$bi]->{'text'}, "\n" );
			$ai++; $bi++;
		} elsif ($op eq 'INS') { $bi++;
		} elsif ($op eq 'DEL') { $ai++;
		} else { die "huh? $op\n"; }
	}
	return @words;
}

sub addToBI {
	my ($words, $ab, $tg) = @_;
	my $breaks = $tg->getAlignmentByName('breaks');
	my $tones = $tg->getAlignmentByName('tones');
	$tones->connectLabels();
	foreach my $word (@{$words}) {
		my $BI = $breaks->getLabelAt($word->{$ab}->getCenter())->{text};
		$word->setProp($ab, 'BI', $BI);
		my $breakLabelCand = $tones->getLabelAt($word->{$ab}->{xmax}-0.011);
		if (abs($breakLabelCand->{xmax} - $word->{$ab}->{xmax}) < .05) {
			# let's just say this is a break mark:
			my $BT = $breakLabelCand->{text};
			if ($BT eq "H") {
				print STDERR "H as BT in $ab for word " . $word->{$ab}->toWavesurferLine() . " with tone label " . $breakLabelCand->toWavesurferLine();
			}
			$word->setProp($ab, 'BT', $BT);
			# the preceding label is the tone-label 
			# iff that label ends during the current word 
			my $toneLabel = $breakLabelCand->getPredecessor();
			my $TO = ($toneLabel->{xmax} - 0.001 >= $word->{$ab}->{xmin}) ? $toneLabel->{text} : '';
			$word->setProp($ab, 'TO', $TO);
		} else {
			$word->setProp($ab, 'BT', ''); # no break-tone
			# if there's no break-tone, then we want the label of 
			# whatever is going on at the start of our word and does not extend after
			my $toneLabel = $tones->getLabelAt($word->{$ab}->{xmin}+0.011);
			my $TO = ($toneLabel->{xmax} + 0.001 <= $word->{$ab}->{xmax} && $toneLabel->{xmax} >= $word->{$ab}->{xmin} + 0.001) ? $toneLabel->{text} : '';
			$word->setProp($ab, 'TO', $TO);
		}
	}
}

# words package: two Label objects ($a and $b) and two hashes for properties ($aprop and $bprop)
package Word;

sub new {
	my ($a, $b) = @_;
	$a->{text} eq $b->{text} or die "different: ", $a->toString(), $b->toString();
	$a->{xmin} = sprintf("%.2f", $a->{xmin});
	$a->{xmax} = sprintf("%.2f", $a->{xmax});
	$b->{xmin} = sprintf("%.2f", $b->{xmin});
	$b->{xmax} = sprintf("%.2f", $b->{xmax});
	return bless { a => $a, b => $b, aprop => {}, bprop => {} };
}

sub setProp {
	my ($self, $ab, $prop, $value) = @_;
	$self->{$ab . "prop"}->{$prop} = $value;
}
