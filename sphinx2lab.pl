#!/usr/bin/perl
use strict;
use warnings;

my $lastEnd = 0.0;
my $unaligned = "";
my $numWords = 0;
my $numAligned = 0;
while (my $line = <>) {
	$numWords++;
	if ($line =~ m/  (\S+)\s+\[(\d+):(\d+)\]$/) {
		my ($label, $start, $end) = ($1, $2, $3);
		$start = sprintf("%.2f", $start / 1000);
		$end = sprintf("%.2f", $end / 1000);
		if ($unaligned ne "") {
			print STDERR "$lastEnd\t$start\t$unaligned\n";
			$unaligned = "";
		}
		print "$start\t$end\t$label\n";
		$lastEnd = $end;
		$numAligned++;
	} else {
		$line =~ m/\- (\S+)/ or die "odd line: $line\n";
		my $label = $1;
		$unaligned .= " $label";
	}
}
print STDERR "$numWords words, $numAligned aligned, proportion: " . $numAligned/$numWords . "\n";
